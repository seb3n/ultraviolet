from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ultraviolet.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'ultraviolet.views.home_page', name='home_page'),
    url(r'^home', 'ultraviolet.views.home_page', name='home_page'),
   # url(r'^apache2', 'ultraviolet.views.apache2_page', name='apache2_page'),
   # url(r'^home_server', 'ultraviolet.views.home_server_page', name='home_server_page'),
   # url(r'^networking', 'ultraviolet.views.networking_page', name='networking_page'),
   # url(r'^snort', 'ultraviolet.views.snort_page', name='snort_page'),
    url(r'^basic_django', 'ultraviolet.views.django_website_page', name='django_website_page'),
   # url(r'^home_website_server', 'ultraviolet.views.home_website_server_page', name='home_website_server_page'),
   # url(r'^hackrf', 'ultraviolet.views.hackrf_page', name='hackrf_page'),
   # url(r'^horn_antenna', 'ultraviolet.views.horn_antenna_page', name='horn_antenna_page'),
   # url(r'^uav', 'ultraviolet.views.uav_page', name='uav_page'),
   # url(r'^smart_house', 'ultraviolet.views.smart_house_page', name='smart_house_page'),
   # url(r'^malware_lab', 'ultraviolet.views.malware_lab_page', name='malware_lab_page'),
   # url(r'^sirius', 'ultraviolet.views.sirius_page', name='sirius_page'),
    
   # url(r'^admin/', include(admin.site.urls)),
)
