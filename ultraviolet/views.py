from  django.shortcuts import render_to_response
from  django.http import Http404
from  django.shortcuts import redirect
from  django.core.urlresolvers import reverse

def  home_page(request):
	# return render_to_response('home.html')
	return render_to_response('index.html')

def apache2_page(request):
	return render_to_response('apache2.html')
	
def home_server_page(request):
	return render_to_response('home_server.html')
	
def networking_page(request):
	return render_to_response('networking.html')
	
def snort_page(request):
	return render_to_response('snort.html')
	
def django_website_page(request):
	return render_to_response('django_tut.html')	
	
def home_website_server_page(request):
	return render_to_response('home_website_server.html')
	
def hackrf_page(request):
	return render_to_response('hackrf.html')
	
def horn_antenna_page(request):
	return render_to_response('horn_antenna.html')
	
def uav_page(request):
	return render_to_response('uav.html')
	
def smart_house_page(request):
	return render_to_response('smart_house.html')
	
def malware_lab_page(request):
	return render_to_response('malware_lab.html')
	
def sirius_page(request):
	return render_to_response('sirius.html')